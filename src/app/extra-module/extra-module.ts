import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExtraRoutingModule } from './extra-routing.module';

import { ExtraComponent } from './extra/extra.component';
import { Extra01Component } from './extra/extra-01/extra-01.component';
import { Extra02Component } from './extra/extra-02/extra-02.component';
import { Extra03Component } from './extra/extra-03/extra-03.component';



@NgModule({
    declarations: [
        ExtraComponent,
        Extra01Component,
        Extra02Component,
        Extra03Component
    ],
    imports: [
        CommonModule,
        ExtraRoutingModule
    ],
    providers: []
})
export class ExtraModule {

}