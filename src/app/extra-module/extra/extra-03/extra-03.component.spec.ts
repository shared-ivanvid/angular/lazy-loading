import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Extra03Component } from './extra-03.component';

describe('Extra03Component', () => {
  let component: Extra03Component;
  let fixture: ComponentFixture<Extra03Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Extra03Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Extra03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
