import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Extra01Component } from './extra-01.component';

describe('Extra01Component', () => {
  let component: Extra01Component;
  let fixture: ComponentFixture<Extra01Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Extra01Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Extra01Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
