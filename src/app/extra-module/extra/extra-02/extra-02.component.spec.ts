import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Extra02Component } from './extra-02.component';

describe('Extra02Component', () => {
  let component: Extra02Component;
  let fixture: ComponentFixture<Extra02Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Extra02Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Extra02Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
