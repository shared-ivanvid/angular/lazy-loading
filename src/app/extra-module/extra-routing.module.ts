import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExtraComponent } from './extra/extra.component';
import { Extra01Component } from './extra/extra-01/extra-01.component';
import { Extra02Component } from './extra/extra-02/extra-02.component';
import { Extra03Component } from './extra/extra-03/extra-03.component';



const extraRoutes: Routes = [
    { path: '', component: ExtraComponent, children: [
        { path: '01', component: Extra01Component },
        { path: '02', component: Extra02Component },
        { path: '03', component: Extra03Component },
    ]}
];

@NgModule({
    imports: [
        RouterModule.forChild(extraRoutes)
    ],
    exports: [RouterModule]
})
export class ExtraRoutingModule {}